import { createRouter, createWebHistory } from 'vue-router'
import store from "../store/index"
import Home from '../views/Home'
import Favourite from '../views/Favourite'
import LogIn from '../views/LogIn'
import SignUp from '../views/SignUp'
import AddProduct from '../views/AddProduct'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/add-product',
    name: 'Add Product',
    component: AddProduct,
    beforeEnter: (to, from, next) => {
      if (store.state.user.name) next()
      else next('Log in')
    }
  },
  {
    path: '/favourite',
    name: 'Favourite',
    component: Favourite,
    beforeEnter: (to, from, next) => {
      if (store.state.user.name) next()
      else next('Log in')
    }
  },
  {
    path: '/log-in',
    name: 'Log In',
    component: LogIn,
    beforeEnter: (to, from, next) => {
      if (store.state.user.name) next('Home')
      else next()
    }
  },
  {
    path: '/sign-up',
    name: 'Sign Up',
    component: SignUp,
    beforeEnter: (to, from, next) => {
      if (store.state.user.name) next('Home')
      else next()
    }
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: { name: 'Home' }
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach(() => {
  store.commit('closeMenu')
})

export default router
