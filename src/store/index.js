import { createStore } from 'vuex'
import router from '../router'

import firebase from 'firebase/app'
import "firebase/firestore"

const filters = {
  namespaced: true,
  state: () => ({
    name: '',
    location: '',
    category: '',
    priceFrom: '',
    priceTo: Infinity
  }),
  getters: {
    name(state) {
      return state.name
    },
    location(state) {
      return state.location
    },
    category(state) {
      return state.category
    },
    priceFrom(state) {
      return state.priceFrom
    },
    priceTo(state) {
      return state.priceTo
    }
  },
  mutations: {
    name(state, payload) {
      state.name = payload
    },
    location(state, payload) {
      state.location = payload
    },
    category(state, payload) {
      state.category = payload
    },
    priceFrom(state, payload) {
      state.priceFrom = payload
    },
    priceTo(state, payload) {
      state.priceTo = payload > 0 ? payload : Infinity
    }
  }
}

export default createStore({
  state: {
    isOpenMenu: false,
    user: {
      id: '',
      name: '',
      email: ''
    },
    products: [],
    categories: [
      'cat1', 'cat2', 'cat3', 'cat4', 'cat5'
    ],
    loveProducts: []
  },
  getters: {
    user(state) {
      return state.user
    },
    userName(state) {
      return state.user.name
    },
    products(state) {
      return state.products
    },
    categories(state) {
      return state.categories
    },
    favourites(state) {
      return state.loveProducts
    }
  },
  mutations: {
    SET_USER(state, data) {
      state.user.id = data.uid
      state.user.email = data.email
      state.user.name = data.displayName
      router.push('Home')
    },
    SET_USER_NAME(state, name){ //becouse sign up not reactive
      state.user.name = name
    },
    setProducts(state, list){
      state.products = []
      list.forEach(item => state.products.push({
        id: item.id,
        title: item.data().title,
        location: item.data().location,
        category: item.data().category,
        descr: item.data().descr,
        price: item.data().price,
        imgUrl: item.data().imgUrl
      }))
    },
    setLove(state, id) {
      let indexEl = state.loveProducts.indexOf(id)
      if(indexEl != -1)
        return state.loveProducts.splice(indexEl, 1)
      state.loveProducts.push(id)
    },
    toggleMenu(state) {
      state.isOpenMenu = !state.isOpenMenu
    },
    closeMenu(state) {
      state.isOpenMenu = false
    }
  },
  actions: {
    fetchUser({ commit }, user) {
      if (user) {
        commit("SET_USER", user)
      }
    },
    getProducts({ commit }) {
      firebase.firestore().collection("products").orderBy("timestamp", 'desc').get().then((querySnapshot) => {
        commit("setProducts", querySnapshot)
      })
    }
  },
  modules: {
    filters: filters,
  }
})
