import 'bootstrap/dist/css/bootstrap.css'
import { createApp } from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import './firebase.js'

import firebase from 'firebase/app'
import "firebase/auth"


firebase.auth().onAuthStateChanged(user => {
  store.dispatch("fetchUser", user)
});

createApp(App).use(store).use(router).mount('#app')
